#!/usr/bin/env python
#Julin Maloof
#April 13, 2012
#June 7, 2012: group SNPs by sequence window
#given a fasta file and a .csv file with chromosomes and positions, extract sequence windows around those locations

import sys
import argparse
import csv
from Bio import SeqIO
from Bio.SeqFeature import SeqFeature, FeatureLocation

def writeSNPregion(chrom, start, stop):
    tmpseq = SeqFeature(FeatureLocation(start,stop)).extract(Seq_dictionary[chrom])
    tmpseq.id = chrom+"_"+str(int((start+stop)/2)) #give the sequence fragment a name
    tmpseq.description = chrom + ":" + str(start) + ".." + str(stop) #add info on start and stop positions
    SeqIO.write(tmpseq, args.output, 'fasta') #write it!



#set up command line arguments
parser = argparse.ArgumentParser(description="Retrieve a window of sequences around a genomic location",
                                 epilog="NOTE: location file must be in .csv format and include the columns 'chr' and 'pos'.\n" +
                                 "Fasta names in input file must match those in the 'chr' column of the location file.\n" +
                                 "File should be sorted on chromosome then position.")

parser.add_argument('-o','--output',type=argparse.FileType('w'),default="-",
                    help='output path.  omit for STDOUT')
parser.add_argument('-v','--verbose',action='store_true')

parserGroups = parser.add_argument_group('required arguments') #because otherwise arguments starting with a - are listed as optional
parserGroups.add_argument('-i','--input',type=argparse.FileType('r'), required=True,
                    help="path to fasta sequences")
parserGroups.add_argument('-l', '--location',type=argparse.FileType('rU'), required=True,
                    help="path to csv file of locations")
parserGroups.add_argument('-w', '--windowSize',type=float,required=True,
                    help="total window size in kb")

#retrieve command line arguments
args = parser.parse_args()
halfWindow = int(round(args.windowSize*1000/2)) #convert from kb to bp and adjust to give half window size

if args.verbose: #writing to STDerr so that I do not write into fasta stream if fasta is going to stdout.
    sys.stderr.write("Half window size is " + str(halfWindow) + " bp\n")

##load sequences into database
#because I am working on Arabidopsis and its genome is small I will just load it into a dictionary
#if we were dealing with a larger genome a different method would be required
#in an ideal world I would check the size of the incoming file and adjust accordingly
Seq_dictionary = SeqIO.to_dict(SeqIO.parse(args.input,'fasta'))

if args.verbose:
    sys.stderr.write(args.input.name + " loaded.\nValid chromosome names are: ")
    sys.stderr.write(str(Seq_dictionary.keys())+"\n")

#open location file
locationReader = csv.reader(args.location)

#read the first line to get the header
header = locationReader.next()
chrIndex = header.index('chr') #find the column that the chromosome info in
posIndex = header.index('pos') #find the column that the SNP position info in


#now step through csv file one line at a time.  Each row corresponds to one SNP
previous_chrom = "NA"
for row in locationReader:
    chrom = row[chrIndex] #chromosome for this SNP
    pos = int(row[posIndex]) #position for this SNP
    if locationReader.line_num == 2: #first data line
        start = max(0,int(pos)-halfWindow) #start position for sequence retrieval.
        stop = min(pos+halfWindow,len(Seq_dictionary[chrom].seq))
    else: #check to see if the we should declare a new region or extend the old one
        if ((stop + halfWindow <= pos) or (chrom != previous_chrom)):
            #this SNP is not in the window of the previous SNP
            #so previous SNP needs to be written and start needs to be reset
            #use the previous chromosome because if we have switched chromosomes we need to reference the old one
            #and if we haven't it will be correct anyway
            writeSNPregion(previous_chrom,start,stop)
            start = max(0,pos - halfWindow) #reset start position for sequence retrieval.
    stop = min(pos+halfWindow,len(Seq_dictionary[chrom].seq))
    previous_chrom = chrom

#Write the final SNP
writeSNPregion(chrom,start,stop)




