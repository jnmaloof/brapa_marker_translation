## Create a CSV file of version 1.1 marker locations for input
## to the "extractSeqWindow.py" script.

data <- read.csv("~/git/brassica_genetic_map/Output/Brassica_F8_v1.0_gen.csv")

head(data[1:10])

names(data)

data <- data[,1:2]

data$pos <- as.numeric(substr(data$id,5,20))

head(data)

data <- data[,-1]

colnames(data)[1] <- "chr"

head(data)

data <- data[order(data$chr,data$pos),]

head(data)

tail(data)

write.csv(data,"MarkerTranlation_1.1_1.5/V1.1_markers.csv",row.names=FALSE)

