# README: Marker Translation

Here we have code and data to translate SNP markers from V1.1 to V1.5 of the B.rapa genome.  

## Output

If all you want is the translation table, then you can download MarkerTranslationTableV1.1_V1.5.csv

## Requirements

If you want to run this yourself you will need:

* Python2.x with [Biopython](http://biopython.org/wiki/Main_Page) installed
* genome fasta files, i.e. from [BRAD](http://brassicadb.org/brad/)
* a working installation of NCBI's blast+ suite
    * Install it from your linux package manager or 
    * Download directly [here](http://blast.ncbi.nlm.nih.gov/Blast.cgi?PAGE_TYPE=BlastDocs&DOC_TYPE=Download)
    
## Procedure

* Use `CreateMarkerCsv.R` to create a list of markers and positions in appropriate format.  This assumes that you are starting from [Cody, Mark, and Mike's genetic map files](https://bitbucket.org/rjcmarkelz/brassica_genetic_map)
* Use `extractSeqWindow.py` to pull out flanking sequence around your SNP from the old genome.  **Important the marker file MUST be sorted by position first**
    * Example (change the paths to match your situation):

           extractSeqWindow.py -i ~/Sequences/ref_genomes/B_rapa/genome/V0830 B.rapa_genome_sequence_0830.fa -l V1.1_markers.csv -w 0.2 -o V1.1_markers.fas

* Use `blastn` to search the new genome with the SNP-flanking regions from the old genome:


        blastn -query V1.1_markers.fas -db ~/Sequences/ref_genomes/B_rapa/genome/V1.5/BrapaV1.5.fas -word_size 100 -dust no -outfmt 10 > V1.1_1.5_blastout.csv
        
* Use `MakeMarkerTranslationTableFromBlast.R` to create the final output table.
